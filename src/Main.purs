module Main where

import Prelude
import Data.Array
import Data.Int (toNumber)
import Math (cos, sin, pi, pow, abs)
import Control.Alternative
import Control.Applicative
import Control.Apply
import Data.Maybe
import Control.Monad.Eff.Exception.Unsafe
import Data.Generic
import OutWatch
import OutWatch.Tags as H
import OutWatch.Attributes (text)
import OutWatch.Core (render) as OutWatch
import Data.String as S


data Expr = Var Char
          | And Expr Expr
          | Or Expr Expr
          | Not Expr
          | Const Boolean


derive instance eqExpr :: Eq Expr

instance showExpr :: Show Expr where
  show (Const x) = show x
  show (Var v) = S.singleton v
  show (Not e) = "¬" <> show e
  show (Or x y) = show x <> " OR " <> show y
  show (And x y) = show x <> " AND " <> show y

freeVar :: Expr -> Maybe Char
freeVar (Const _) = Nothing
freeVar (Var v) = Just v
freeVar (Not e) = freeVar e
freeVar (Or x y) = freeVar x <|> freeVar y
freeVar (And x y) = freeVar x <|> freeVar y

subVar :: Char -> Expr -> Expr -> Expr
subVar _ _ (Const b) = Const b
subVar c e (Var v)   = if v == c then e else Var v
subVar c e (Not x)   = Not (subVar c e x)
subVar c e (Or x y)  = Or  (subVar c e x) (subVar c e y)
subVar c e (And x y) = And (subVar c e x) (subVar c e y)

guessVar :: Char -> Boolean -> Expr -> Expr
guessVar c b = subVar c (Const b)

simplify :: Expr -> Expr
simplify (Const b) = Const b
simplify (Var v) = Var v
simplify (Not e) =
  case simplify e of
    Const b -> Const (not b)
    e -> Not e
simplify (Or x y) =
  let es = filter (_ /= Const false) [simplify x, simplify y]
  in
    -- If True is in a branch, the entire expression is True.
    if Const true `elem` es
    then Const true
    else
      case es of
        -- If all the values were False, this 'or' is unsatisfied.
        [] -> Const false
        [e] -> e
        [e1, e2] -> Or e1 e2
        _ -> unsafeThrow "Makes no sense"
-- Dual to the simplify (Or x y) definition.
simplify (And x y) =
  let es = filter (_ /= Const true) [simplify x, simplify y]
  in
    if Const false `elem` es
    then Const false
    else
      case es of
        [] -> Const true
        [e] -> e
        [e1, e2] -> And e1 e2
        _ -> unsafeThrow "Makes no sense"


-- Extract the boolean from the Const constructor.
unConst :: Expr -> Boolean
unConst (Const b) = b
unConst _ = unsafeThrow "Not const"


satisfiable :: Expr -> Boolean
satisfiable expr =
  case freeVar expr of
    Nothing -> unConst expr
    Just v ->
      -- We have a variable to guess.
      -- Construct the two guesses.
      -- Return whether either one of them works.
      let trueGuess  = simplify (guessVar v true expr)
          falseGuess = simplify (guessVar v false expr)
      in satisfiable trueGuess || satisfiable falseGuess


reqs :: Array Expr
reqs = [And (Not (Var 'x')) (Var 'y'),
        Or (Not (Var 'y')) (Var 'z'),
        Not (Var 'z')]

conj :: Array Expr -> Expr
conj = foldl And (Const true)

main = render "#controls" $
  H.div [H.h1 [text "These are some abstract requirements:"],
         H.ol ((\x -> H.li [text x]) <$> (show <$> reqs)),
         H.h1 [text "And I have determined that they are"],
         H.h1 [text (if satisfiable (conj reqs) then "Satisfiable" else "Not Satisfiable")],
         H.h2 [text "I'm sorry I do not give more information but I am just a stupid app."]]
